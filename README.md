# TOML Odin

Bindings to [tomlc99](https://github.com/cktan/tomlc99) in the [Odin programming language](https://odin-lang.org).

You'll need to copy `libtoml.a` to the package directory to link correctly.

## License

BSD 3-clause.
